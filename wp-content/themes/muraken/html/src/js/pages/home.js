(($) => {
    const testing_js = () => {
        const $selector = $("#event-click");
        $selector.on("click", (e) => {
            let $this = e.currentTarget;
            console.log($this, "testing");
        });
    };
    $(() => {
        testing_js();
        $("select").select2({
            minimumResultsForSearch: -1,
            containerCssClass: "select2-border-container",
            dropdownCssClass: "select2-border-dropdown"
        });

    });
})(jQuery);