let autoprefix = require('gulp-autoprefixer');
let babel = require('gulp-babel');
let browserSync = require('browser-sync').create();
let cleanCSS = require('gulp-clean-css');
let concat = require('gulp-concat');
let del = require('del');
let gulp = require('gulp');
let plumber = require('gulp-plumber');
let Pug = require('gulp-pug');
let rename = require('gulp-rename');
let Sass = require('gulp-sass');
let sourcemaps = require('gulp-sourcemaps');
let uglify = require('gulp-uglify');
let debug = require('gulp-debug');
let Watch = require('gulp-watch');
let Purgecss = require('gulp-purgecss');

let PORT = 5000;
let VERSION = '2.5.0';

let SRC = {
    root: 'src',
    pug: '/pug',
    pug_watch: '',
    css: '/scss',
    js: '/js',
    img: '/images',
    private: '/private',
    building: false
};

let DIST = {
    clean: ['dist', '*.html', './src/scss/*.css', './src/js/plugins.min.js', './src/js/main.min.js'],
    root: './',
    pug: '',
    css: 'src/scss',
    js: 'src/js',
    img: 'src/images',
    assets: '/assets'
};

// Clear all
let clean = function () {
    return del(DIST.clean);
};
let browser_sync = function () {
    return browserSync.init({
        server: {
            baseDir: './',
            serveStaticOptions: { extensions: ['html'] }
        },
        startPath: SRC.pug_watch,
        port: PORT,
        notify: false,
        timestamps: true,
        files: [
            // DIST.root + SRC.pug_watch + "/**/*.html",
            DIST.root + DIST.css + '/{style,plugins}.css',
            DIST.root + DIST.js + '/plugins.min.js',
            DIST.root + DIST.js + '/pages.js'
        ]
    });
};
let pug = function (obj) {
    let listDefault = [
        SRC.root + SRC.pug + SRC.pug_watch + '/**/*.pug',
        '!' + SRC.root + SRC.pug + '/**/_*.pug',
        '!' + SRC.root + SRC.pug + '/include/**/*.pug'
    ];

    return gulp
        .src(obj !== undefined && obj.path !== undefined ? obj.path : listDefault, {
            base: SRC.root + SRC.pug + SRC.pug_watch,
            allowEmpty: true
        })
        .pipe(
            plumber({
                errorHandler: function (err) {
                    // eslint-disable-next-line no-console
                    console.log(err);
                }
            })
        )
        .pipe(debug({ title: '[HTML] ', showCount: SRC.building, showFiles: true }))
        .pipe(Pug({ pretty: '    ' }))
        .pipe(gulp.dest(DIST.root + DIST.pug + SRC.pug_watch))
        .on('end', () => browserSync.reload());
};
let sass = function () {
    return gulp
        .src([SRC.root + SRC.css + '/{style,plugins}.scss'])
        .pipe(sourcemaps.init())
        .pipe(debug({ title: '[CSS]  ', showCount: SRC.building, showFiles: SRC.building }))
        .pipe(Sass().on('error', Sass.logError))
        .pipe(autoprefix())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(DIST.root + DIST.css))
        .pipe(cleanCSS())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest(DIST.root + DIST.css));
};
let purgecss = function () {
    return gulp
        .src([DIST.root + DIST.css + '/plugins.min.css'])
        .pipe(
            Purgecss({
                content: [DIST.root + '/*.html', DIST.root + DIST.js + '/plugins.min.js', DIST.root + DIST.js + '/pages.min.js'],
                rejected: false,
                whitelistPatterns: [/tooltip/]
            })
        )
        .pipe(gulp.dest(DIST.root + DIST.css));
};
let scripts_library = function () {
    return (
        gulp
            .src([SRC.root + SRC.js + '/**/*.min.js', '!' + SRC.root + SRC.js + '/plugins.min.js', '!' + SRC.root + SRC.js + '/pages.min.js'])
            // .pipe(babel({ presets: ['@babel/env'] }))
            .pipe(debug({ title: '[JSPI] ', showCount: SRC.building, showFiles: true }))
            .pipe(
                uglify().on('error', function (e) {
                    // eslint-disable-next-line no-console
                    console.log(e);
                })
            )
            .pipe(concat('plugins.min.js'))
            .pipe(gulp.dest(DIST.root + DIST.js))
    );
};

let scripts_main = function () {
    return gulp
        .src([SRC.root + SRC.js + '/main.js', SRC.root + SRC.js + '/edit.js', SRC.root + SRC.js + '/pages/*.js'])
        .pipe(
            plumber({
                errorHandler: function (err) {
                    // eslint-disable-next-line no-console
                    console.log(err);
                }
            })
        )
        .pipe(debug({ title: '[JS] ', showCount: SRC.building, showFiles: true }))
        .pipe(babel({ presets: ['@babel/env'] }))
        .pipe(concat('pages.js'))
        .pipe(gulp.dest(DIST.root + DIST.js))
        .pipe(
            uglify().on('error', function (e) {
                // eslint-disable-next-line no-console
                console.log(e);
            })
        )
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest(DIST.root + DIST.js));
};

let timeOutSass;

let watch = function () {
    Watch(
        [
            SRC.root + SRC.pug + SRC.pug_watch + '/**/*.pug'
            // SRC.root + SRC.pug + "/components/**/*.pug",
            // SRC.root + SRC.pug + "/layouts/**/*.pug"
        ],
        obj => (obj.path.match(/[-_\w]+[.][\w]+$/i)[0].indexOf('_') === 0 ? pug() : pug(obj))
    );
    Watch([SRC.root + SRC.css + '/**/*.scss'], obj => {
        // eslint-disable-next-line no-console
        console.log(`[SCSS] ${obj.path.match(/[-_\w]+[.][\w]+$/i)[0]}`);
        clearTimeout(timeOutSass);
        timeOutSass = setTimeout(sass, 700);
    });
    Watch([SRC.root + SRC.js + '/**/*.min.js', '!' + SRC.root + SRC.js + '/plugins.min.js', '!' + SRC.root + SRC.js + '/pages.js', '!' + SRC.root + SRC.js + '/pages.min.js'], scripts_library);
    Watch([SRC.root + SRC.js + '/edit.js', SRC.root + SRC.js + '/main.js', SRC.root + SRC.js + '/pages/*.js'], scripts_main);
    // Bonus image & copy file

    // eslint-disable-next-line no-console
    console.log('\x1b[31m\x1b[1m\n================== \t COREFE: ' + VERSION + ' / START PORT: ' + PORT + ' \t ================== \n\x1b[0m');
};

let _build = function () {
    SRC.pug_watch = '';
    SRC.building = true;
    return del(DIST.clean);
};

/*
 * You can use CommonJS `exports` module notation to declare tasks
 */
exports.clean = clean;
exports.sass = sass;
exports.pug = pug;
exports.scripts_library = scripts_library;
exports.scripts_main = scripts_main;
exports.purgecss = purgecss;

/*
 * Return the task when a file changes
 */
exports.watch = gulp.series(
    clean,
    gulp.parallel(sass, scripts_library, scripts_main, pug),
    purgecss,
    gulp.parallel(
        gulp.parallel(browser_sync, watch)
    )
);

/*
 * Define default task that can be called by just running `gulp` from cli
 */
exports.default = gulp.series(
    _build,
    gulp.parallel([sass, scripts_library, scripts_main, pug]),
    purgecss
);
